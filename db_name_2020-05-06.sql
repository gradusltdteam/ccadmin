# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.17)
# Database: db_name
# Generation Time: 2020-05-06 14:17:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table asset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `asset`;

CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL,
  `is_auto_update` tinyint(1) NOT NULL,
  `is_liability` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;

INSERT INTO `asset` (`id`, `status`, `name`, `type`, `is_auto_update`, `is_liability`, `created_at`, `updated_at`)
VALUES
	(1,0,'Cash',0,0,0,'2020-03-03 09:23:46','2020-03-03 09:23:46');

/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table asset_price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `asset_price`;

CREATE TABLE `asset_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `amount` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `comment` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table auth_assignment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`)
VALUES
	('admin','2',1588769950),
	('admin','3',1588774526);

/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`)
VALUES
	('admin',1,'',NULL,NULL,1588769950,1588770953);

/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table auth_item_child
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table auth_rule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;

INSERT INTO `event` (`id`, `created_at`, `title`, `text`)
VALUES
	(1,'2020-03-03 00:00:00','ewew','');

/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table log_price_api_update
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log_price_api_update`;

CREATE TABLE `log_price_api_update` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `asset_id` int(11) NOT NULL,
  `url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `result` text,
  `api_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `log_price_api_update` WRITE;
/*!40000 ALTER TABLE `log_price_api_update` DISABLE KEYS */;

INSERT INTO `log_price_api_update` (`id`, `status`, `created_at`, `asset_id`, `url`, `result`, `api_key`)
VALUES
	(1,0,'2020-04-24 09:28:01',14,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=N6AG3LKONI0H47KV','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19679000\",\"6. Last Refreshed\":\"2020-04-24 09:28:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19681000\",\"9. Ask Price\":\"0.19682000\"}}',''),
	(2,0,'2020-04-24 09:28:02',14,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=N6AG3LKONI0H47KV','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19679000\",\"6. Last Refreshed\":\"2020-04-24 09:28:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19681000\",\"9. Ask Price\":\"0.19682000\"}}',''),
	(3,0,'2020-04-24 09:28:09',14,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=N6AG3LKONI0H47KV','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19679000\",\"6. Last Refreshed\":\"2020-04-24 09:28:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19681000\",\"9. Ask Price\":\"0.19682000\"}}',''),
	(4,0,'2020-04-24 09:28:11',14,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=N6AG3LKONI0H47KV','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19679000\",\"6. Last Refreshed\":\"2020-04-24 09:28:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19681000\",\"9. Ask Price\":\"0.19682000\"}}',''),
	(5,0,'2020-04-24 09:29:02',14,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=N6AG3LKONI0H47KV','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19670000\",\"6. Last Refreshed\":\"2020-04-24 09:29:02\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19670000\",\"9. Ask Price\":\"0.19672000\"}}',''),
	(93,0,'2020-04-09 21:23:34',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19790000\",\"6. Last Refreshed\":\"2020-04-09 21:23:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19789000\",\"9. Ask Price\":\"0.19790000\"}}',''),
	(94,0,'2020-04-09 21:23:36',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40390555\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(95,0,'2020-04-09 21:23:37',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7281.66000000\",\"6. Last Refreshed\":\"2020-04-09 21:23:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7282.17000000\",\"9. Ask Price\":\"7282.48000000\"}}',''),
	(96,0,'2020-04-09 21:23:39',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7351\",\"05. price\":\"1211.4500\",\"06. volume\":\"2158305\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(97,1,'2020-04-09 21:23:41',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=APPL&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(98,0,'2020-04-10 00:10:49',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19831000\",\"6. Last Refreshed\":\"2020-04-10 00:10:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19827000\",\"9. Ask Price\":\"0.19831000\"}}',''),
	(99,0,'2020-04-10 00:10:50',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(100,0,'2020-04-10 00:10:52',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7270.54000000\",\"6. Last Refreshed\":\"2020-04-10 00:10:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7270.54000000\",\"9. Ask Price\":\"7270.92000000\"}}',''),
	(101,0,'2020-04-10 00:10:54',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(102,1,'2020-04-10 00:10:58',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Note\":\"Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day. Please visit https:\\/\\/www.alphavantage.co\\/premium\\/ if you would like to target a higher API call frequency.\"}',''),
	(103,1,'2020-04-10 00:28:56',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(104,0,'2020-04-10 00:28:57',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19808000\",\"6. Last Refreshed\":\"2020-04-10 00:28:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19805000\",\"9. Ask Price\":\"0.19808000\"}}',''),
	(105,0,'2020-04-10 00:28:59',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(106,0,'2020-04-10 00:29:01',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7271.77000000\",\"6. Last Refreshed\":\"2020-04-10 00:29:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7271.80000000\",\"9. Ask Price\":\"7272.48000000\"}}',''),
	(107,0,'2020-04-10 00:29:02',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(108,1,'2020-04-10 00:51:48',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(109,1,'2020-04-10 00:51:51',10,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=RUR&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for CURRENCY_EXCHANGE_RATE.\"}',''),
	(110,0,'2020-04-10 00:51:53',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19830000\",\"6. Last Refreshed\":\"2020-04-10 00:51:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19829000\",\"9. Ask Price\":\"0.19830000\"}}',''),
	(111,0,'2020-04-10 00:51:54',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(112,0,'2020-04-10 00:51:56',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7284.33000000\",\"6. Last Refreshed\":\"2020-04-10 00:51:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7284.33000000\",\"9. Ask Price\":\"7285.60000000\"}}',''),
	(113,1,'2020-04-10 00:59:02',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(114,0,'2020-04-10 00:59:04',10,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=RUB&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"RUB\",\"2. From_Currency Name\":\"Russian Ruble\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.01342200\",\"6. Last Refreshed\":\"2020-04-10 00:59:03\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"-\",\"9. Ask Price\":\"-\"}}',''),
	(115,0,'2020-04-10 00:59:05',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(116,0,'2020-04-10 00:59:06',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19835000\",\"6. Last Refreshed\":\"2020-04-10 00:59:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19834000\",\"9. Ask Price\":\"0.19836000\"}}',''),
	(117,0,'2020-04-10 00:59:08',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(118,0,'2020-04-10 01:04:38',10,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=RUB&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"RUB\",\"2. From_Currency Name\":\"Russian Ruble\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.01340700\",\"6. Last Refreshed\":\"2020-04-10 01:04:38\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"-\",\"9. Ask Price\":\"-\"}}',''),
	(119,0,'2020-04-10 01:04:40',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(120,0,'2020-04-10 01:04:41',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19831000\",\"6. Last Refreshed\":\"2020-04-10 01:04:05\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19830000\",\"9. Ask Price\":\"0.19831000\"}}',''),
	(121,0,'2020-04-23 19:35:53',10,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=RUB&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"RUB\",\"2. From_Currency Name\":\"Russian Ruble\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.01333400\",\"6. Last Refreshed\":\"2020-04-23 19:35:52\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"-\",\"9. Ask Price\":\"-\"}}',''),
	(122,0,'2020-04-23 19:35:54',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1245.5400\",\"03. high\":\"1285.6100\",\"04. low\":\"1242.0000\",\"05. price\":\"1263.2100\",\"06. volume\":\"2061121\",\"07. latest trading day\":\"2020-04-22\",\"08. previous close\":\"1216.3400\",\"09. change\":\"46.8700\",\"10. change percent\":\"3.8534%\"}}',''),
	(123,0,'2020-04-23 19:35:56',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19553000\",\"6. Last Refreshed\":\"2020-04-23 19:35:02\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19553000\",\"9. Ask Price\":\"0.19556000\"}}',''),
	(124,0,'2020-04-25 21:28:39',14,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=N6AG3LKONI0H47KV','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19421000\",\"6. Last Refreshed\":\"2020-04-25 21:28:06\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19416000\",\"9. Ask Price\":\"0.19422000\"}}',''),
	(125,0,'2020-04-28 08:49:00',14,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=W5LCQHG7UFED8LNL','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19689000\",\"6. Last Refreshed\":\"2020-04-28 08:48:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19688000\",\"9. Ask Price\":\"0.19689000\"}}','');

/*!40000 ALTER TABLE `log_price_api_update` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;

INSERT INTO `migration` (`version`, `apply_time`)
VALUES
	('Da\\User\\Migration\\m000000_000001_create_user_table',1588766985),
	('Da\\User\\Migration\\m000000_000002_create_profile_table',1588766987),
	('Da\\User\\Migration\\m000000_000003_create_social_account_table',1588766989),
	('Da\\User\\Migration\\m000000_000004_create_token_table',1588766991),
	('Da\\User\\Migration\\m000000_000005_add_last_login_at',1588766991),
	('Da\\User\\Migration\\m000000_000006_add_two_factor_fields',1588766993),
	('Da\\User\\Migration\\m000000_000007_enable_password_expiration',1588766994),
	('Da\\User\\Migration\\m000000_000008_add_last_login_ip',1588766996),
	('Da\\User\\Migration\\m000000_000009_add_gdpr_consent_fields',1588766998),
	('m000000_000000_base',1588766969),
	('m140506_102106_rbac_init',1588767322),
	('m160816_175556_init',1588766999),
	('m170907_052038_rbac_add_index_on_auth_assignment_user_id',1588767323),
	('m180523_151638_rbac_updates_indexes_without_prefix',1588767324);

/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `created_at`, `title`, `text`)
VALUES
	(1,'2020-03-03 00:00:00','ewew','');

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table profile
# ------------------------------------------------------------

DROP TABLE IF EXISTS `profile`;

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_profile_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `timezone`, `bio`)
VALUES
	(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table social_account
# ------------------------------------------------------------

DROP TABLE IF EXISTS `social_account`;

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_social_account_provider_client_id` (`provider`,`client_id`),
  UNIQUE KEY `idx_social_account_code` (`code`),
  KEY `fk_social_account_user` (`user_id`),
  CONSTRAINT `fk_social_account_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table token
# ------------------------------------------------------------

DROP TABLE IF EXISTS `token`;

CREATE TABLE `token` (
  `user_id` int(11) DEFAULT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `created_at` int(11) NOT NULL,
  UNIQUE KEY `idx_token_user_id_code_type` (`user_id`,`code`,`type`),
  CONSTRAINT `fk_token_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table transfer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transfer`;

CREATE TABLE `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `amount` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `amount_cash` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `comment` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `transfer` WRITE;
/*!40000 ALTER TABLE `transfer` DISABLE KEYS */;

INSERT INTO `transfer` (`id`, `asset_id`, `amount`, `amount_cash`, `comment`, `created_at`)
VALUES
	(25,0,0.00000000,100000.00000000,'TopUp','2020-01-01'),
	(43,NULL,0.00000000,50000.00000000,'top up 2','2020-05-06'),
	(47,NULL,0.00000000,5000.00000000,'topup3','2020-05-06');

/*!40000 ALTER TABLE `transfer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table transfer_
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transfer_`;

CREATE TABLE `transfer_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `amount` decimal(15,8) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  `pair_transfer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `transfer_` WRITE;
/*!40000 ALTER TABLE `transfer_` DISABLE KEYS */;

INSERT INTO `transfer_` (`id`, `asset_id`, `amount`, `comment`, `created_at`, `pair_transfer_id`)
VALUES
	(25,1,100000.00000000,'TopUp','2020-01-01',NULL),
	(26,14,29850.00000000,NULL,'2020-02-15',27),
	(27,1,-10000.00000000,NULL,'2020-02-15',26),
	(39,16,10000.00000000,NULL,'2020-05-04',40),
	(40,1,-50000.00000000,NULL,'2020-05-04',39);

/*!40000 ALTER TABLE `transfer_` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `confirmed_at` int(11) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `updated_at` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `last_login_at` int(11) DEFAULT NULL,
  `last_login_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_tf_key` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_tf_enabled` tinyint(1) DEFAULT '0',
  `password_changed_at` int(11) DEFAULT NULL,
  `gdpr_consent` tinyint(1) DEFAULT '0',
  `gdpr_consent_date` int(11) DEFAULT NULL,
  `gdpr_deleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_username` (`username`),
  UNIQUE KEY `idx_user_email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `unconfirmed_email`, `registration_ip`, `flags`, `confirmed_at`, `blocked_at`, `updated_at`, `created_at`, `last_login_at`, `last_login_ip`, `auth_tf_key`, `auth_tf_enabled`, `password_changed_at`, `gdpr_consent`, `gdpr_consent_date`, `gdpr_deleted`)
VALUES
	(2,'admin','alex.kirs@gmail.com','$2y$10$bgpDNT3vVXbdLaPwYzyLy.JGOPL/Y0kAwU76a6FNXz/TtIPCCXosu','4po0NUre5U8zG3TpnpPH8XV7QlpqQXb6',NULL,NULL,0,1588769950,NULL,1588769950,1588769950,1588771684,'172.22.0.1','3L4WERVFG7GT2FUB',1,1588769950,0,NULL,0),
	(3,'Testvo','v@alfa.im','$2y$10$.yIGJBhkOxGcVfDPmTSBae0K5L3ZcLv1XfO7tCk0Oy5pahg6gGq5O','9gbN9sTW2myZptYcGIvzqcJjm0uBQAPN',NULL,'172.22.0.1',0,1588774500,NULL,1588774501,1588774501,1588774554,'172.22.0.1','',0,1588774501,0,NULL,0);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
