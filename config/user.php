<?php


return [
    'class' => Da\User\Module::class,
    // ...other configs from here: [Configuration Options](installation/configuration-options.md), e.g.
    // 'administrators' => ['admin'], // this is required for accessing administrative actions
    // 'generatePasswords' => true,
    // 'switchIdentitySessionKey' => 'myown_usuario_admin_user_key',
    'enableRegistration' => false,
    'enableEmailConfirmation' => false,
    'enableTwoFactorAuthentication' => true,
    // 'administrators' => ['admin'],
    'administratorPermissionName' => 'admin',
];
