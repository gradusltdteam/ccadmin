<?php

namespace app\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\db\Expression;
use app\models\Transfer;
use app\models\Asset;
use app\models\AssetPrice;
use app\models\Profit;
use app\models\Event;
use app\models\News;
use yii\data\ActiveDataProvider;
use app\models\FilterStatistic;

class LiabilityController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $query = Asset::find();
        $query->where('is_liability = 1');

        $assetProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort'=> ['defaultOrder' => new Expression('name DESC')],
            ]
        );


        //transfers
        //all liabilities id
        $ids = Asset::find()->where('is_liability = 1')->select('id')->column();

        $query = Transfer::find()
            ->where(['asset_id' => $ids])
        ;

        $transferProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
            ]
        );

        return $this->render('index', [
            'assetProvider' => $assetProvider,
            'transferProvider' => $transferProvider,
        ]);
    }
}
