#!/usr/bin/env bash

function finish_cleanup {
    #stops running test containers in any case
    docker-compose stop
    docker-compose rm -f
}
trap finish_cleanup EXIT

# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

#will not attempt to use the Docker CLI for interactive run and exec operations
export COMPOSE_INTERACTIVE_NO_CLI=1

export HTTP_EXTERNAL_PORT=8008

docker-compose up -d

docker-compose exec -T php composer install

#TODO: provide placeholders & substitution for user/pswd/db from .env file?

docker-compose exec -T db mysql -u root -proot --database=yii -e \
'CREATE DATABASE IF NOT EXISTS yii_test CHARACTER SET utf8; GRANT ALL PRIVILEGES ON yii_test.* TO yii@"%";'

docker-compose exec -T php bash -c \
"chmod a+w web/assets/ && tests/bin/yii migrate --interactive=0"

docker-compose exec -T sphinx bash -c \
"export YII_TEST=true && indexer --all --rotate && searchd"

docker-compose exec -T php vendor/bin/codecept run --html tests/_output/tests_result.html

#cleanup of running containers is performed inside finish_cleanup function that would be triggered at script finish
#or in case if error occurs

git fetch origin
git checkout master
git config --local user.email "teamcity@ufobe.com"
git config --local user.name "TeamCity Auto Merge"
git merge develop
git push origin master