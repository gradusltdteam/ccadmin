#!/usr/bin/env bash
#this file will be executed on plain (i.e. w/o docker) production server by CI system

# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

#prevent script execution from invalid path
cd "$(dirname "${BASH_SOURCE[0]}")/.."

git fetch origin
git stash
git reset --hard origin/master

php composer install

chmod a+w ./web/assets/
./yii migrate --interactive=0
