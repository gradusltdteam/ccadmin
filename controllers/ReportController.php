<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Report;
use yii\data\ActiveDataProvider;

class ReportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'report',
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $query = Report::find()
        ;

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id = null)
    {
        $query = News::find()
            ->where(['id' => $id])
        ;
        $model = $query->one();

        if(!$model){
            $model = new News;
        }

        if($model->load(Yii::$app->request->post()) && $model->save())
        {
            $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $query = Report::find()
            ->where(['id' => $id])
        ;
        if($model = $query->one()) {
            $model->delete();
        }
        $this->redirect(['index']);
    }

}
