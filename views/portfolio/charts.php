<?php

/**
 * @var $this \yii\web\View
 * @var $dateFrom string
 * @var $dateTo string
 */

use app\assets\GCExternalAsset;
use app\models\Statistic;
use yii\helpers\Json;
use yii\helpers\Url;

GCExternalAsset::register($this);
list($origLoggedDate, $loggedData) = Statistic::getLoggedData($dateFrom, $dateTo, true);

$this->registerJs('var userStatsUrl = "' . Url::to(['/admin/users-statistic'], true) . '"', $this::POS_HEAD);
$this->registerJs('var dataLogged = ' . Json::encode($loggedData), $this::POS_HEAD);
$this->registerJs('var origLoggedDate = ' . Json::encode($origLoggedDate), $this::POS_HEAD);
?>

<div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">Chart</div>
                <div class="panel-body text-center">
                    <div id="chart-logged"></div>
                </div>
            </div>
        </div>
    <?php if (count($loggedData) > 2) { ?>
    <?php } ?>

</div>
