<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use yii\base\Action;

class AfterRequest extends Action
{
    public static function run()
    {
        $execution_time = (microtime(true) - $GLOBALS['time_start'])/60;
        \Yii::info($_SERVER['REQUEST_URI'].' finished '.'('.($execution_time).')', 'app\BeforeRequest');
    }
}
