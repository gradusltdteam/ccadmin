<?php

use yii\db\Migration;

/**
 * Class m201129_173938_fix1
 */
class m201129_173938_fix1 extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(<<<'EOF'
        TRUNCATE `log_price_api_update`;

        ALTER TABLE `log_price_api_update`
        ADD INDEX `asset_id_created_at` (`asset_id`, `created_at`);
        
        ALTER TABLE `transfer`
        ADD INDEX `asset_id_created_at` (`asset_id`, `created_at`);

        ALTER TABLE `asset_price`
        ADD INDEX `asset_id_created_at` (`asset_id`, `created_at`);
    EOF
);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201129_173938_fix1 cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201129_173938_fix1 cannot be reverted.\n";

        return false;
    }
    */
}
