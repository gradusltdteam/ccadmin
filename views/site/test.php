TEST
<pre>
<?php
echo "  php:";
echo date('Y-m-d H:i:s');
echo " (" . date_default_timezone_get().")";
echo "\n";
echo "mysql:";
$expression = new \yii\db\Expression('NOW()');
$now = (new \yii\db\Query)->select($expression)->scalar();  // SELECT NOW();
echo $now;

$expression = new \yii\db\Expression('@@session.time_zone');
$time_zone = (new \yii\db\Query)->select($expression)->scalar();
echo " (" . $time_zone.")";
?>
</pre>