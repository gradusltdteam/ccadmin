<?php

namespace app\models;

use Yii;

class Report extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
//    public static function tableName()
//    {
//        return 'comment';
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'required'],
            [['id'], 'integer'],
            [['title'], 'string'],
            [['query'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

}
