<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

class Asset extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
//    public static function tableName()
//    {
//        return 'comment';
//    }

    const CASH_ID = 1;

    const TYPE_CRYPTO = 0;
    const TYPE_STOCK = 1;
    
    const TYPE = [
        Asset::TYPE_CRYPTO => 'Crypto',
        Asset::TYPE_STOCK => 'Stock',
    ];

    public function init()
    {
        // no search method is available in Gii generated Non search class
        if (!method_exists($this, 'search')) {
            $this->is_auto_update = true;
        }

        parent::init();
    }

    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['name','comment'], 'string'],
            [['name'], 'required'],
            [['id', 'status', 'type', 'is_auto_update'], 'integer'],
            // ['is_auto_update', 'default', 'value' => true],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }
    
    public function beforeDelete()
    {
        Transfer::deleteAll(['asset_id' => $this->id]);
        AssetPrice::deleteAll(['asset_id' => $this->id]);

        //@todo Cleanup other tables with asset_id

        return parent::beforeDelete();
    }

    public static function getOrCreateCash()
    {
        //@todo maybe do something more configurable here
        $query = Asset::find()
            ->where(['id' => Asset::CASH_ID])
        ;

        if (!$cash = $query->one()) {
            $cash = new Asset;
            $cash->id = Asset::CASH_ID;
            $cash->name = 'Cash';
            $cash->save();
        }
        return $cash;
    }

    public function getAmount($date = null)
    {
        $query = (new \yii\db\Query())
        ->from(Transfer::tableName())
            ;

        if($this->id == Asset::CASH_ID) {
            $query->select(new \yii\db\Expression('COALESCE(SUM(amount_cash),0)'));
        } else {
            $query->select(new \yii\db\Expression('COALESCE(SUM(amount),0)'));
            $query->where('{{asset_id}} = :asset_id', [':asset_id' => $this->id]);
        }

        if ($date) {
            $query->andWhere('{{created_at}} <= :date', [':date' => $date]);
        }

        $value = $query->scalar();

        return $value;
    }

    public function getPrice($date = null)
    {
        if ($this->id === Asset::CASH_ID) {
            return 1;
        }

        $query = (new \yii\db\Query())
        ->select(new \yii\db\Expression('amount'))
        ->from(AssetPrice::tableName())
        ->where('{{asset_id}} = :asset_id', [':asset_id' => $this->id])
        ->orderBy('created_at DESC')
        ->limit(1)
            ;

        if ($date) {
            $query->andWhere('{{created_at}} <= :date', [':date' => $date]);
        }

        $value = $query->scalar();

        return $value;
    }

    public function getValue($date = null)
    {
        return $this->getAmount($date) * $this->getPrice($date);
    }

    public function getShare($date = null)
    {
        $valueSum = 0;
        $assets = Asset::find()
            ->where('is_liability = :is_liability', [':is_liability' => $this->is_liability])
            ->all();
        foreach ($assets as $asset) {
            $valueSum += $asset->getValue($date);
        }
        return $valueSum!=0?$this->getValue($date)/$valueSum:0;
    }

    public function getYield($date = null)
    {
        // $command = Yii::$app->db->createCommand("SELECT SUM(amount) FROM profit WHERE asset_id = :asset_id");
        // $command->bindValue(':asset_id', $this->id);
        $yield = $this->getProfitSum($this->id);

        return $yield;
    }

    public function getTransfers($start = null, $end = null)
    {
        $query = Transfer::find()
            ->where('{{asset_id}} = :asset_id', [':asset_id' => $this->id])
            ;

        if ($start) {
            $query->andWhere('{{created_at}} >= :start', [':start' => $start]);
        }
        if ($end) {
            $query->andWhere('{{created_at}} <= :end', [':end' => $end]);
        }

        return $query->all();
    }

    /*
    public static function getTransferSum($asset_id = null, $start = null, $end = null)
    {
        //@todo for some reason we ignored $start for transfers....
        // $command = Yii::$app->db->createCommand("SELECT COALESCE(SUM(amount),0) FROM transfer WHERE created_at >= :start AND created_at <= :end");

        $query = (new \yii\db\Query())
        ->select(new \yii\db\Expression('COALESCE(SUM(amount),0)'))
        ->from(Transfer::tableName())
            ;

        if ($asset_id) {
            $query->andWhere('{{asset_id}} = :asset_id', [':asset_id' => $asset_id]);
        }
        //@todo for some reason we ignored $start for transfers....
        // if($start) {
        //     $query->andWhere('{{created_at}} >= :start', [':start' => $start]);
        // }
        if ($end) {
            $query->andWhere('{{created_at}} <= :end', [':end' => $end]);
        }

        return $query->scalar();
    }
    */

    /*
        $asset_id - id, or 'assets', or 'liabilities'
    */
    public static function getProfitSum($asset_id = null, $start = null, $end = null)
    {
        //@todo add profit calculation
        /*
            получить amount на start * price на start
            получить все трансферы за период start - end
                по каждому трасферу получить price на момент покупки ??? непонятно как это учитывать
            получить amount на end * price на end

            profit = (price * amount start) - (price * amount end)
        */
        if (!$end) {
            $end = date("Y-m-d", strtotime("today"));
        }

        $query = Asset::find();
        $query->andWhere('{{id}} != :cash_id', [':cash_id' => Asset::CASH_ID]);

        if ($asset_id) {
            if($asset_id == 'asset') {
                $query->andWhere('is_liability = 0');
            } elseif($asset_id == 'liability') {
                $query->andWhere('is_liability = 1');
            } else {
                $query->andWhere('{{id}} = :asset_id', [':asset_id' => $asset_id]);
            }
        }
        $assets = $query->all();

        $profit = 0;

        foreach ($assets as $asset) {
            //start of period
            if ($start) {
                $value = $asset->getValue($start);
                $profit += -$value;
            }

            // end of period
            $value = $asset->getValue($end);
            $profit += +$value;

            //transactions for period
            $transfers = $asset->getTransfers($start, $end);

            foreach ($transfers as $transfer) {
                $profit += $transfer->getAmountUsd();
            }
        }

        return $profit ;
    }

    /*
        $asset_id - id, or 'assets', or 'liabilities'
    */
    public static function getValueSum($asset_id = null, $date = null)
    {

        $query = Asset::find();
        // $query->andWhere('{{id}} != :cash_id', [':cash_id' => Asset::CASH_ID]);

        if ($asset_id) {
            if($asset_id == 'asset') {
                $query->andWhere('is_liability = 0');
            } elseif($asset_id == 'liability') {
                $query->andWhere('is_liability = 1');
            } elseif($asset_id == 'no cash') {
                $query->andWhere('{{id}} != :asset_id', [':asset_id' => Asset::CASH_ID]);
            } else {
                $query->andWhere('{{id}} = :asset_id', [':asset_id' => $asset_id]);
            }
        }
        $assets = $query->all();

        $value = 0;

        foreach ($assets as $asset) {
            $value += $asset->getValue($date);
        }

        return $value;
    }

    public function updatePrice($created_at = null, $amount = null, $price_id = null)
    {
        $price = AssetPrice::find()
            ->where(['id' => $price_id])
            ->one();

        if (!$amount) {
            $amount = 0;
        }
        if (!$created_at) {
            $created_at = date('Y-m-d');
        }

        //don't create record for specific date if already exists. update existing one
        if (!$price && $created_at) {
            $price = AssetPrice::find()
                ->where([
                    'asset_id' => $this->id,
                    'created_at' => $created_at,
                ])
                ->one();
        }

        if (!$price) {
            $price = new AssetPrice;
            $price->asset_id = $this->id;
        }

        $price->amount = $amount;
        $price->created_at = $created_at;

        // var_dump($price);
        // var_dump($price->save());
        // var_dump($price->errors);
        // die();

        $price->save();

        return $price;
    }

    public function updateApiPrice()
    {
        //@todo handle API errors
        // $apikey = 'JZG02LP6DM8WG4I9';
        // $apikey = 'N6AG3LKONI0H47KV';
        $apikey = Yii::$app->params['api_key'];
        
        
        // $apikey = 'demo';

        if ($this->type == Asset::TYPE_CRYPTO) {
            $from_currency = $this->name;
            $to_currency = 'USD';
            // $from_currency = 'BTC';
            // $to_currency = 'CNY';


            $url = 'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency='.$from_currency.'&to_currency='.$to_currency.'&apikey='.$apikey;
            $json = file_get_contents($url);
            $obj = json_decode($json);
    
            // var_dump($obj);
            // die();
            $price = $obj->{"Realtime Currency Exchange Rate"}->{"5. Exchange Rate"} ?? 0;
        }

        if ($this->type == Asset::TYPE_STOCK) {
            $symbol = $this->name;
            // $symbol = 'IBM';

            $url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol='.$symbol.'&apikey='.$apikey;
            $json = file_get_contents($url);
            $obj = json_decode($json);
    
            // var_dump($obj);
            // die();
            $price = $obj->{"Global Quote"}->{"05. price"} ?? 0;
        }

        //update price
        $log = new LogPriceApiUpdate;
        $log->asset_id = $this->id;

        $log->url = $url;
        $log->result = json_encode($obj);
        
        // @todo update only if we get valid price from API
        if ($price) {
            $this->updatePrice(null, $price);
            $log->status = LogPriceApiUpdate::STATUS_NORMAL;
        } else {
            $log->status = LogPriceApiUpdate::STATUS_ERROR;
        }

        $log->save();
        //echo $obj->access_token;
    }

    public function getLastPriceApiUpdateTime()
    {
        return LogPriceApiUpdate::find()
            ->select('created_at')
            ->where('asset_id = ' . $this->id . ' AND status = ' . LogPriceApiUpdate::STATUS_NORMAL)
            ->orderBy('created_at DESC')
            ->limit(1)
            ->scalar();
    }

    public static function updateAllApiPrice()
    {
        $result = new \stdclass;
        $result->results = [];
        try {
            // @todo cleamup old LogPriceApiUpdate records
            
            //get assets that are not updated recently
            $assets = Asset::find()
                ->where('id !=' . Asset::CASH_ID .' AND is_auto_update')
                ->all();

            $result->results[] = count($assets) . " assets found. updating price...\n";
            
            //foreach assets get last update date, order by last update date
            $asset_array = [];
            foreach ($assets as $asset) {
                $asset_array_item = [];
                $asset_array_item['model'] = $asset;
                $asset_array_item['last_update_time'] = $asset->getLastPriceApiUpdateTime();

                $asset_array[] = $asset_array_item;
            }

            usort($asset_array, function ($a, $b) {
                // compare numbers only
                return strcmp($a['last_update_time'], $b['last_update_time']);
            });

            // @todo maybe add extra condition - don't update asset more then every XXX minutes
            
            //cycle through assets
            foreach ($asset_array as $asset_array_item) {
                $asset = $asset_array_item['model'];
                //check API limits
                //up to 5 API requests per minute and 500 requests per day
                // + soft limit 15 request/hour to spread update during day

                //5/min
                // $count = LogPriceApiUpdate::find()
                //     ->where('created_at >= (NOW() - INTERVAL 5 MINUTE)')
                //     ->count();

                // if ($count >= 5) {
                //     throw new \Exception('API 5 calls/minute limit');
                // }

                //30/min
                $count = LogPriceApiUpdate::find()
                ->where('created_at >= (NOW() - INTERVAL 1 MINUTE)')
                ->count();

                if ($count >= 30) {
                    throw new \Exception('API 30 calls/minute limit');
                }
            
                //15/hour
                // $count = LogPriceApiUpdate::find()
                // ->where('created_at >= (NOW() - INTERVAL 1 HOUR)')
                // ->count();

                // if ($count >= 15) {
                //     throw new \Exception('API 15 calls/hour limit');
                // }

                //500/day
                // $count = LogPriceApiUpdate::find()
                //     ->where('created_at >= (NOW() - INTERVAL 24 HOUR)')
                //     ->count();

                // if ($count >= 500) {
                //     throw new \Exception('API 500 calls/24h limit');
                // }
                
                //@todo do we need to get price date from API?
                $asset->updateApiPrice();
            }
        } catch (\Exception $e) {
            $result->error = $e->getMessage();
            $result->error .= ' (on line ' .$e->getLine().' of '.$e->getFile().')';
        }

        return $result;
    }

    public static function getAssetData($custom_value = null)
    {
        //http://nasdaqtrader.com/dynamic/SymDir/nasdaqlisted.txt
        //https://www.alphavantage.co/physical_currency_list/
        //https://www.alphavantage.co/digital_currency_list/

        $result = [];

        //digital_currency_list
        $key = 'digital_currency_list';
        $data = Yii::$app->cache->get($key);

        if ($data === false) {
            $string = file_get_contents('https://www.alphavantage.co/digital_currency_list/');
            $csv = array_map('str_getcsv', explode("\n", $string));
            
            array_shift($csv); # remove column header
            $data = [];
            foreach ($csv as $line) {
                if(!isset($line[0]) || !isset($line[1])) {
                    continue;
                }
                $data[$line[0]] = $line[0] . ' - ' . $line[1];
            }
            
            Yii::$app->cache->set($key, $data, 60 * 60 * 24);
        }
        
        $result['Digital Currencies'] = $data;

        //physical_currency_list
        $key = 'physical_currency_list';
        $data = Yii::$app->cache->get($key);

        if ($data === false) {
            $string = file_get_contents('https://www.alphavantage.co/physical_currency_list/');
            $csv = array_map('str_getcsv', explode("\n", $string));
            
            array_shift($csv); # remove column header
            $data = [];
            foreach ($csv as $line) {
                if(!isset($line[0]) || !isset($line[1])) {
                    continue;
                }
                $data[$line[0]] = $line[0] . ' - ' . $line[1];
            }
                
            Yii::$app->cache->set($key, $data, 60 * 60 * 24);
        }
        $result['Physical Currencies'] = $data;

        //Nasdaq-listed securities
        $key = 'nasdaq-list';
        $data = Yii::$app->cache->get($key);

        if ($data === false) {
            $string = file_get_contents('http://nasdaqtrader.com/dynamic/SymDir/nasdaqlisted.txt');
            $csv = array_map(function ($str) {
                return str_getcsv($str, '|');
            }, explode("\n", $string));
                
            array_shift($csv); # remove column header
            $data = [];
            foreach ($csv as $line) {
                if(!isset($line[0]) || !isset($line[1])) {
                    continue;
                }
                $data[$line[0]] = $line[0] . ' - ' . $line[1];
            }

            Yii::$app->cache->set($key, $data, 60 * 60 * 24);
        }

        $result['Nasdaq-listed securities'] = $data;
        
        // var_dump($result);
        // die();

        //add custom data if it is not present in any lists
        if($custom_value) {
            $custom_value_found = false;
            foreach($result as $list) {
                if(isset($list[$custom_value])) {
                    $custom_value_found = true;
                    break;
                }
            }
            if(!$custom_value_found) {
                $result[$custom_value] = $custom_value;
            }
        }



        return $result;
    }
}
