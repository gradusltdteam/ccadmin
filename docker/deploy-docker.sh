#!/usr/bin/env bash
##this file will be executed on dockerized production server by CI system

# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

#will not attempt to use the Docker CLI for interactive run and exec operations
export COMPOSE_INTERACTIVE_NO_CLI=1

#prevent script execution from invalid path
cd "$(dirname "${BASH_SOURCE[0]}")/.."

git fetch origin
git stash
git reset --hard origin/master

docker-compose exec -T php composer install

chmod a+w ./web/assets/
docker-compose exec -T php ./yii migrate --interactive=0

#docker-compose  exec -T sphinx indexer --all --rotate
#docker-compose  exec -T sphinx searchd