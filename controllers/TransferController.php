<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Transfer;

class TransferController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'ajax',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionAjax()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $result = new \stdclass;
        $id = Yii::$app->request->post('pk');
        $attribute = Yii::$app->request->post('name');
        $value = Yii::$app->request->post('value');

        $model = Transfer::find()->where(['id' => $id])->one();
        if($model) {
            $model[$attribute] = $value;
            $model->save();
            $result->id = $model->id;
            $result->text = 'saved';
        }

        return $result;
    }
}
