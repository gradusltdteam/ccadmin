<?php

namespace app\models;

use Yii;

class Transfer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
//    public static function tableName()
//    {
//        return 'comment';
//    }

    //used in forms
    public $amount_usd;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at','comment'], 'safe'],
            [['id'], 'integer'],
            [['amount_usd','amount','amount_cash'], 'number'],
            [['created_at'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Date',
        ];
    }

    public function getAsset()
    {
        return $this->hasOne(Asset::className(), ['id' => 'asset_id']);
    }

    public function getAmountUsd()
    {
        return $this->amount_cash;
    }


}
