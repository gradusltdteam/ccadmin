<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Event;
use yii\data\ActiveDataProvider;

class EventController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'update',
                            'delete',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionIndex()
    {
        $query = Event::find()
        ;

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id = null)
    {
        $query = Event::find()
            ->where(['id' => $id])
        ;
        $model = $query->one();

        if(!$model){
            $model = new Event;
        }

        if($model->load(Yii::$app->request->post()) && $model->save())
        {
            $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $query = Event::find()
            ->where(['id' => $id])
        ;
        if($model = $query->one()) {
            $model->delete();
        }
        $this->redirect(['index']);
    }

}
