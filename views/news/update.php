<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;

$this->title = 'News '.($model->isNewRecord?'Create':'Update #'.$model->id);
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-news">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin([]); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $form->field($model, 'title')->label(true)->textInput(['placeholder' => 'Title']) ?>
                    <?= $form->field($model, 'text')->widget(\yii\redactor\widgets\Redactor::className()) ?>
                    <?= $form->field($model, 'created_at')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter date ...'],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                        ]
                    ]);?>

                    <hr>
                    <div class="text-center">
                        <div class="btn-group" role="group">
                            <?= Html::submitButton('<i class="fa fa-check"></i> Submit', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?php $form::end(); ?>
        </div>
    </div>
</div>
