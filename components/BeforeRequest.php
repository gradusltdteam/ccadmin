<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\components;

use yii\base\Action;

class BeforeRequest extends Action
{
    public static function run()
    {
        \Yii::info($_SERVER['REQUEST_URI'], 'app\BeforeRequest');
        $GLOBALS['time_start'] = microtime(true);

    }
}
