<?php

use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-news">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?php
            echo GridView::widget(
                [
                    'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <div class='row'>
                                        <div class='col-md-12 text-right'>
                                            
                                        </div>
                                    </div>

                                    <hr>

                                    {items}


                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover'
                    ],
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        [
                            'class' => CheckboxColumn::className()
                        ],
                        'id',
                        'created_at:date',
                        'title',
                        'query',
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{apply} {delete}',
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                            'buttons' => [
                                'apply' => function ($url, $model, $key) {
                                    return Html::a('<i class="fa fa-play"></i> Apply', [
                                        'report',
                                        'query' => $model->query,
                                    ], ['class' => 'btn btn-xs btn-success']
                                    );
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fa fa-trash"></i> Delete', [
                                        'delete',
                                        'id' => $model->id,
                                    ], ['class' => 'btn btn-xs btn-danger delete-button', 'data-id' => $model->id]
                                    );
                                }
                            ]
                        ]
                    ]
                ]
            );
            ?>


        </div>
    </div>
</div>
