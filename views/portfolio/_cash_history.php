<?php
use yii\grid\GridView;
use yii\grid\CheckboxColumn;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use app\models\Asset;

echo GridView::widget([
    'layout' => "
            <div class='panel panel-default'>
                <div class='panel-body'>
                    <legend>Cash History</legend>
                    {items}


                    <hr>
                    <div class='row'>
                        <div class='col-md-6'>{summary}</div>
                        <div class='col-md-6 text-right'>{pager}</div>
                    </div>
                </div>
            </div>

    ",
    'tableOptions' => [
        'class' => 'table table-striped table-hover'
    ],
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => CheckboxColumn::className()
        ],
        'id',
        [
            'attribute' => 'asset_id',
            'label' => 'Asset',
            'format' => 'raw',
            'value' => function ($model) {
                if (!$model->asset) {
                    $cash = Asset::getOrCreateCash();
                    return $cash->name;
                }

                $return = $model->asset->name;
                return $return;
            }
        ],
        [
            'label' => 'Amount',
            'format' => 'decimal',
            'value' => function ($model) {
                return $model->amount;
            }
        ],
        [
            'label' => 'Amount USD',
            'format' => 'currency',
            'attribute' => 'amount',
            'value' => function ($model) {
                return $model->amount_cash;
            }
        ],
        [
            'label' => 'Price USD (calculated)',
            'format' => 'currency',
            //'attribute' => 'amount',
            'value' => function ($model) {
                //@todo move method to model?
                return $model->amount_cash != 0 ? $model->amount / -$model->amount_cash : 0;
            }
        ],
        'comment',
        'created_at:date',
        [
            'class' => ActionColumn::className(),
            'template' => '{delete}',
            'contentOptions' => [
                'class' => 'text-right'
            ],
            'buttons' => [
                'delete' => function ($url, $model, $key) {
                    return Html::a(
                        '<i class="fa fa-trash"></i> Delete',
                        [
                        '/portfolio/transfer-delete',
                        'transfer_id' => $model->id
                    ],
                        ['class' => 'btn btn-xs btn-danger delete-button', 'data-id' => $model->id]
                    );
                }
            ]
        ]
    ]
]);
