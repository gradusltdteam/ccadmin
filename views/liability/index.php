<?php

use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use app\models\Transfer;
use app\models\Asset;

$this->title = 'Liabilities';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liability-index">
    <h1><?= Html::encode($this->title) ?>
    </h1>

    <div class="row">
        <div class="col-lg-12">
            <?php
            echo GridView::widget(
    [
                    'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <div class='row'>
                                        <div class='col-md-12 text-right'>
                                            " . Html::a(
                        '<i class="fa fa-plus"></i> Add new',
                        ['/portfolio/asset-update', 'is_liability' => 1],
                        ['class' => 'btn btn-success']
                    ) . "
                                        </div>
                                    </div>

                                    <hr>

                                    {items}


                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover'
                    ],
                    'dataProvider' => $assetProvider,
                    'columns' => [
                        [
                            'class' => CheckboxColumn::className()
                        ],
                        'name',
                        'comment',
                        [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Asset::TYPE[$model->type];
                            }
                        ],
                        'amount:decimal',
                        'price:currency',
                        'value:currency',
                        [
                            'attribute' => 'share',
                            'format' => ['percent', 2],
                        ],
                        // 'created_at:date',
                        [
                            'header' => 'Price Updated',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $result = $model->is_auto_update ? HTML::tag('div', 'auto', [
                                    'class' => 'label label-info',
                                ]) . ' ' : '';

                                $log = \app\models\LogPriceApiUpdate::find()
                                ->where('asset_id = ' . $model->id)
                                ->orderBy('created_at DESC')
                                ->limit(1)
                                ->one();
                                
                                if ($log) {
                                    if ($log->status == \app\models\LogPriceApiUpdate::STATUS_ERROR) {
                                        $result .= HTML::tag('div', 'error ' . Yii::$app->formatter->format($log->created_at, 'relativeTime'), [
                                            'class' => 'label label-danger',
                                            'title' => $log->result,
                                        ]);
                                    } else {
                                        $result .= HTML::tag('div', Yii::$app->formatter->format($log->created_at, 'relativeTime'), [
                                            'class' => 'label label-default',
                                        ]);
                                    }
                                }

                                return $result;
                            }
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update} {delete} {transfer} {price}',
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    if ($model->id === 1) {
                                        return false;
                                    }
                                    return Html::a(
                                        '<i class="fa fa-edit"></i> Edit',
                                        [
                                        '/portfolio/asset-update',
                                        'id' => $model->id,
                                        'is_liability' => 1,
                                    ],
                                        ['class' => 'btn btn-xs btn-primary']
                                    );
                                },
                                'delete' => function ($url, $model, $key) {
                                    if ($model->id === 1) {
                                        return false;
                                    }
                                    return Html::a(
                                        '<i class="fa fa-trash"></i> Delete',
                                        [
                                        '/portfolio/asset-delete',
                                        'id' => $model->id
                                    ],
                                        ['class' => 'btn btn-xs btn-danger delete-button', 'data-id' => $model->id]
                                    );
                                },
                                'profit' => function ($url, $model, $key) {
                                    if ($model->id === 1) {
                                        return false;
                                    }
                                    return Html::a(
                                        '<i class="fa fa-check"></i> Profit',
                                        [
                                        '/portfolio/profit',
                                        'id' => $model->id
                                    ],
                                        ['class' => 'btn btn-xs btn-primary', 'data-id' => $model->id]
                                    );
                                },
                                'transfer' => function ($url, $model, $key) {
                                    if ($model->id === 1) {
                                        return false;
                                    }
                                    return Html::a(
                                        '<i class="fa fa-check"></i> Transfer',
                                        [
                                        '/portfolio/transfer',
                                        'id' => $model->id
                                    ],
                                        ['class' => 'btn btn-xs btn-success', 'data-id' => $model->id]
                                    );
                                },
                                'price' => function ($url, $model, $key) {
                                    if ($model->id === 1) {
                                        return false;
                                    }
                                    return Html::a(
                                        '<i class="fa fa-check"></i> Price',
                                        [
                                        '/portfolio/price',
                                        'id' => $model->id
                                    ],
                                        ['class' => 'btn btn-xs btn-default', 'data-id' => $model->id]
                                    );
                                },
                            ]
                        ]
                    ]
                ]
);
            ?>

        </div>
        <div class="row">
            <div class="col-lg-4">
            </div>

            <div class="col-lg-8">

                <?php echo $this->context->renderPartial('/portfolio/_cash_history', ['dataProvider' => $transferProvider]);?>

            </div>
        </div>


    </div>
</div>