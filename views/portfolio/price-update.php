<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\date\DatePicker;

$this->title = 'Price '.($price->isNewRecord?'Create':'Update #'.$price->id);
$this->params['breadcrumbs'][] = ['label' => 'Portfolio', 'url' => ['index']];;
$this->params['breadcrumbs'][] = ['label' => 'Price '.$asset->name, 'url' => ['price', 'id'=>$asset->id]];;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-price">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?php $model = $price?>
            <?php $form = ActiveForm::begin([]); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?= $form->field($model, 'amount')->label(true)->textInput(['placeholder' => 'Amount']) ?>
                    <?= $form->field($model, 'created_at')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter date ...'],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                            'convertFormat' => true,
                        ]
                    ]);?>

                    <hr>
                    <div class="text-center">
                        <div class="btn-group" role="group">
                            <?= Html::submitButton('<i class="fa fa-check"></i> Submit', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?php $form::end(); ?>
        </div>
    </div>
</div>
