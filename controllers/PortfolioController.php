<?php

namespace app\controllers;

use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\db\Expression;
use app\models\Transfer;
use app\models\Asset;
use app\models\AssetPrice;
use app\models\Profit;
use app\models\Event;
use app\models\News;
use yii\data\ActiveDataProvider;
use app\models\FilterStatistic;

class PortfolioController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => [
                            'dashboard',
                            'index',
                            'profit',
                            'profit-update',
                            'profit-delete',
                            'price',
                            'price-update',
                            'price-delete',
                            'asset-update',
                            'asset-delete',
                            'transfer',
                            'transfer-delete',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionDashboard()
    {
        $query = Asset::find();
        $query->where('is_liability = 0');

        $assetProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        $cash = Asset::getOrCreateCash();

        $totalProfit = Asset::getProfitSum('asset');
        $totalValue = Asset::getValueSum('asset');
        $totalLiabilityValue = Asset::getValueSum('liability');
        //var_dump($totalTransfer); die();

        $filter = new FilterStatistic();

        $query = News::find();
        $newsProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        $query = Event::find();
        $eventProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

//        var_dump($weekProfit, $monthProfit, $yearProfit);
//        var_dump($weekTransfer, $monthTransfer, $yearTransfer);
//        die();

        //profit table
        //yesterday
        $date = date("Y-m-d", strtotime("-1 days"));
        $date1 = date("Y-m-d", strtotime("today"));

        $yesterdayProfit = Asset::getProfitSum('asset', $date, null);
        $yesterdayValue = Asset::getValueSum('asset', $date);

        // $asset = Asset::find()->where(['id' => 14])->one();


        // var_dump($date);
        // var_dump($yesterdayValue);
        // var_dump($asset->getValue($date));
        // $yesterday1Value = Asset::getValueSum(null, $date1);
        // var_dump($date1);
        // var_dump($yesterday1Value);
        // var_dump($asset->getValue($date1));
        // die();

        //last week
        $date = date("Y-m-d", strtotime("-7 days"));

        $weekProfit = Asset::getProfitSum('asset', $date, null);
        $weekValue = Asset::getValueSum('asset', $date);

        //last month
        $date = date("Y-m-d", strtotime("-30 days"));

        $monthProfit = Asset::getProfitSum('asset', $date, null);
        $monthValue = Asset::getValueSum('asset', $date);

        //last year
        $date = date("Y-m-d", strtotime("-12 months"));

        $yearProfit = Asset::getProfitSum('asset', $date, null);
        $yearValue = Asset::getValueSum('asset', $date);

        //all
        $allProfit = Asset::getProfitSum('asset');
        
        //Year To Date (YTD)
        $date = date("Y-m-d", strtotime("first day of jan this year"));

        $ytdProfit = Asset::getProfitSum('asset', $date, null);
        $ytdValue = Asset::getValueSum('asset', $date);


        $data = ['allModels' => [
        [
            '<strong>Value</strong>',
            sprintf("%01.2f USD", ($totalValue))
        ],
        [
            'Yesterday',
            sprintf("%01.2f USD", ($yesterdayProfit))
            . ' ('
            . sprintf("%.2f%%", ($yesterdayValue != 0?($yesterdayProfit)/$yesterdayValue:1) * 100)
            . ')'
        ],
        [
            'Week',
            sprintf("%01.2f USD", ($weekProfit))
            . ' ('
            . sprintf("%.2f%%", ($weekValue != 0?($weekProfit)/$weekValue:1) * 100)
            . ')'
        ],
        [
            'Month',
            sprintf("%01.2f USD", ($monthProfit))
            . ' ('
            . sprintf("%.2f%%", ($monthValue != 0?($monthProfit)/$monthValue:1) * 100)
            . ')'
        ],
        [
            'Year',
            sprintf("%01.2f USD", ($yearProfit))
            . ' ('
            . sprintf("%.2f%%", ($yearValue != 0?($yearProfit)/$yearValue:1) * 100)
            . ')'
        ],
        [
            'YTD',
            sprintf("%01.2f USD", ($ytdProfit))
            . ' ('
            . sprintf("%.2f%%", ($ytdValue != 0?($ytdProfit)/$ytdValue:1) * 100)
            . ')'
        ],
        [
            'All',
            sprintf("%01.2f USD", ($allProfit))
        ],
        // [
        //     'value' => $totalValue,
        //     'week' => ($weekValue != 0?($totalValue-$weekValue)/$weekValue:1),
        //     'month' => ($monthValue!=0?($totalValue-$monthValue)/$monthValue:1),
        //     'year' => ($yearValue!=0?($totalValue-$yearValue)/$yearValue:1),
        //     'all' => ($totalProfit)/100,
        //     'ytd' => ($ytdValue!=0?($totalValue-$ytdValue)/$ytdValue:1),
        // ],
        // [
        //     'value' => 'debug:value',
        //     'week' => ($weekValue),
        //     'month' => ($monthValue),
        //     'year' => ($yearValue),
        //     'all' => (''),
        //     'ytd' => (''),
        // ],
    ],
        ];
        $profitProvider = new ArrayDataProvider($data);

        return $this->render('dashboard', [
            'assetProvider' => $assetProvider,
            'profitProvider' => $profitProvider,
            'totalValue' => $totalValue,
            'totalLiabilityValue' => $totalLiabilityValue,
            'totalProfit' => $totalProfit,
            'filter' => $filter,
            'newsProvider' => $newsProvider,
            'eventProvider' => $eventProvider,
        ]);
    }

    public function actionIndex()
    {
        $query = Asset::find();
        $query->where('is_liability = 0');

        $assetProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort'=> ['defaultOrder' => new Expression('name DESC')],
            ]
        );

        $cash = Asset::getOrCreateCash();

        $new_cash = new Transfer;
        $new_cash->asset_id = $cash->id;

        if ($new_cash->load(Yii::$app->request->post()) && $new_cash->save()) {
            //@todo fix this shit - get correct data
            $new_cash->asset_id = null;
            // $new_cash->amount_cash = $new_cash->amount;
            // $new_cash->amount = 0;
            $new_cash->save();
        }
        //var_dump($new_cash->errors);

        $query = Transfer::find()
            //->where(['asset_id' => $cash->id])
        ;
        $transferProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
            ]
        );

        $cashSum = $cash->value;


        return $this->render('index', [
            'assetProvider' => $assetProvider,
            'transferProvider' => $transferProvider,
            'cashSum' => $cashSum,
            'new_cash' => $new_cash,
        ]);
    }

    public function actionTransfer($id)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;

        $asset = $query->one();

        $query = Transfer::find()
            ->where(['asset_id' => $asset->id])
            ->orderBy('created_at DESC, id DESC')
        ;

        $transferProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        $cash = Asset::getOrCreateCash();

        $transfer = new Transfer;
        $transfer->asset_id = $asset->id;

        if ($transfer->load(Yii::$app->request->post()) && $transfer->save()) {
            if ($cash->id != $asset->id) {
                //@todo Temporary fix. If we don't have price - update it
                if (!$asset->getPrice($transfer->created_at)) {
                    $price = $transfer->amount_usd / $transfer->amount;
                    $price = round($price, 8, PHP_ROUND_HALF_DOWN);
                    $asset->updatePrice($transfer->created_at, $price);
                }

            }
            $transfer->amount_cash = -$transfer->amount_usd;

            //@todo not like it
            if($asset->is_liability) {
                $transfer->amount_cash = -$transfer->amount_cash;
            }

            $transfer->save();

            $this->redirect(['transfer', 'id'=>$asset->id]);
        }

        return $this->render('transfer', [
            'asset' => $asset,
            'transfer' => $transfer,
            'transferProvider' => $transferProvider,
        ]);
    }

    public function actionProfit($id)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;

        $asset = $query->one();

        $query = Profit::find()
            ->where(['asset_id' => $asset->id])
        ;

        $profitProvider = new ActiveDataProvider(
            [
                'query' => $query
            ]
        );

        return $this->render('profit', [
            'asset' => $asset,
            'profitProvider' => $profitProvider,
        ]);
    }

    public function actionProfitUpdate($id, $profit_id=null)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;
        $asset = $query->one();

        $query = Profit::find()
            ->where(['id' => $profit_id])
        ;
        $profit = $query->one();

        if (!$profit) {
            $profit = new Profit;
            $profit->asset_id = $asset->id;
        }

        if ($profit->load(Yii::$app->request->post()) && $profit->save()) {
            $this->redirect(['profit', 'id'=>$asset->id]);
        }



        return $this->render('profit-update', [
            'asset' => $asset,
            'profit' => $profit,
        ]);
    }

    public function actionProfitDelete($id, $profit_id=null)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;
        $asset = $query->one();

        $query = Profit::find()
            ->where(['id' => $profit_id])
        ;
        if ($profit = $query->one()) {
            $profit->delete();
        }
        $this->redirect(['profit', 'id'=>$asset->id]);
    }

    public function actionPrice($id)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;

        $asset = $query->one();

        $query = AssetPrice::find()
            ->where(['asset_id' => $asset->id])
        ;

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
                'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
            ]
        );

        return $this->render('price', [
            'asset' => $asset,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPriceUpdate($id, $price_id = null)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;
        $asset = $query->one();

        if ($price_id) {
            $price = AssetPrice::find()
                ->where(['id' => $price_id])
                ->one();
        } else {
            $price = new AssetPrice;
        }

        if ($price->load(Yii::$app->request->post()) && $price->validate()) {
            $created_at = Yii::$app->request->post('AssetPrice')['created_at'] ?? null;
            $amount = Yii::$app->request->post('AssetPrice')['amount'] ?? null;

            $price = $asset->updatePrice(
                $created_at,
                $amount,
                $price_id,
            );

            $this->redirect(['price', 'id'=>$asset->id]);
        }
        
        return $this->render('price-update', [
            'asset' => $asset,
            'price' => $price,
        ]);
    }

    public function actionPriceDelete($id, $price_id=null)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;
        $asset = $query->one();

        $price = AssetPrice::find()
        ->where([
            'id' => $price_id,
        ])
        ->one();

        if ($price) {
            $price->delete();
        }

        $this->redirect(['price', 'id' => $asset->id]);
    }

    public function actionAssetUpdate($id=null)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;
        $asset = $query->one();

        if (!$asset) {
            $asset = new Asset;
            if(Yii::$app->request->get('is_liability')){
                $asset->is_liability = 1;
            }
        }

        if ($asset->load(Yii::$app->request->post()) && $asset->save()) {
            if($asset->is_liability) {
                $this->redirect(['/liability/index']);
            } else {
                $this->redirect(['/portfolio/index']);
            }
        }

        return $this->render('asset-update', [
            'asset' => $asset,
        ]);
    }

    public function actionAssetDelete($id)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;
        if ($asset = $query->one()) {
            $asset->delete();
        }

        if($asset->is_liability) {
            $this->redirect(['/liability/index']);
        } else {
            $this->redirect(['/portfolio/index']);
        }
    }

    public function actionTransferDelete($id=null, $transfer_id=null)
    {
        $query = Asset::find()
            ->where(['id' => $id])
        ;
        $asset = $query->one();

        $query = Transfer::find()
            ->where(['id' => $transfer_id])
        ;
        if ($transfer = $query->one()) {
            $asset = $transfer->asset;
            $transfer->delete();
        }

        if ($id && $asset) {
            //called from asset page
            $this->redirect(['transfer', 'id'=>$asset->id]);
        } elseif($asset) {
            //only transfer_id provided
            if($asset->is_liability) {
                $this->redirect(['/liability/index']);
            } else {
                $this->redirect(['/portfolio/index']);
            }
            // $this->redirect(['index']);
        } else {
            //cash transfer, no asset
            $this->redirect(['/portfolio/index']);
        }
    }
}
