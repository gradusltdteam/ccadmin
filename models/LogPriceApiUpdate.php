<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;


class LogPriceApiUpdate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    const STATUS_NORMAL = 0;
    const STATUS_ERROR = 1;

    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => false,
                'value' => new Expression('NOW()')
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['created_at'], 'required'],
            // [['id'], 'integer'],
            // [['title'], 'string'],
            // [['text'], 'string'],
        ];
    }
}
