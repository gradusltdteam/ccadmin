<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Asset '.($asset->isNewRecord?'Create':'Update '.$asset->name);
$this->params['breadcrumbs'][] = ['label' => 'Portfolio', 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-asset">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-12">
            <?php $model = $asset?>
            <?php $form = ActiveForm::begin([]); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php //echo $form->field($model, 'name')->label(true)->textInput(['placeholder' => 'Name']) ?>
                    <?php echo $form->field($model, 'name')->widget(kartik\select2\Select2::classname(), [
                        'data' => \app\models\Asset::getAssetData($model->name),
                        'options' => ['placeholder' => 'Select an asset ...'],
                        'pluginOptions' => [
                            // 'allowClear' => true,
                            // 'selectOnBlur' => true,
                            // 'selectOnClose' => true,
                            'tags' => true,
                            // 'createTag' => 'function (params) {
                            //     return {
                            //       id: params.term,
                            //       text: params.term
                            //     }
                            //   }',
                        ],
                    ]);?>

                    
                    <?php echo $form->field($model, 'type')->dropDownList(\app\models\Asset::TYPE); ?>
                    <?php echo $form->field($model, 'is_auto_update')->checkbox(); ?>
                    <?php echo $form->field($model, 'comment')->inline(); ?>

                    <hr>
                    <div class="text-center">
                        <div class="btn-group" role="group">
                            <?= Html::submitButton('<i class="fa fa-check"></i> Submit', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?php $form::end(); ?>
        </div>
    </div>
</div>
