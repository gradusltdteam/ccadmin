<?php

use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Transfer from/to '.$asset->name;
$this->params['breadcrumbs'][] = ['label' => 'Portfolio', 'url' => ['index']];;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-transfer">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-lg-4">
            <?php $model = $transfer?>
            <?php $form = ActiveForm::begin([]); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php echo $form->field($model, 'amount_usd')->label(true)->textInput(['placeholder' => 'Amount: positive if from cash, negative if to cash']) ?>
                    <?= $form->field($model, 'amount')->label(true)->textInput(['placeholder' => 'Asset currency']) ?>
                    <?= $form->field($model, 'created_at')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter date ...'],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                        ]
                    ]);?>

                    <hr>
                    <div class="text-center">
                        <div class="btn-group" role="group">
                            <?= Html::submitButton('<i class="fa fa-check"></i> Submit', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?php $form::end(); ?>
            </div>
        </div>

        <div class="col-lg-8">
            <?php
            echo GridView::widget(
                [
                    'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <legend>History</legend>

                                    {items}

                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover'
                    ],
                    'dataProvider' => $transferProvider,
                    'columns' => [
                        [
                            'class' => CheckboxColumn::className()
                        ],
                        'id',
                        // 'amount_usd:currency',
                        [
                            'header' => 'Amount (Asset)',
                            'format' => 'decimal',
                            'attribute' => 'amount',
                        ],
                        [
                            'header' => 'Amount (USD)',
                            'format' => 'currency',
                            'value' => function($model) {
                                return $model->amount_cash;
                            }
                        ],
                        [
                            'header' => 'Rate',
                            'value' => function($model) {
                                return -$model->amount ? $model->amount_cash / $model->amount : 0;
                            }
                        ],
                        // [
                        //     'attribute' => 'amount',
                        //     'value' => function($model) {
                        //         return $model->amount;
                        //     }
                        // ],
                        'created_at:date',
                        [
                            'class' => \yii2mod\editable\EditableColumn::class,
                            'attribute' => 'comment',
                            'url' => ['/transfer/ajax'],
                         ],                        
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{delete}',
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                            'buttons' => [
                                'delete' => function ($url, $model, $key) use ($asset) {
                                    return Html::a('<i class="fa fa-trash"></i> Delete', [
                                        'transfer-delete',
                                        'id' => $asset->id,
                                        'transfer_id' => $model->id,
                                    ], ['class' => 'btn btn-xs btn-danger delete-button', 'data-id' => $model->id]
                                    );
                                }
                            ]
                        ]
                    ]
                ]
            );
            ?>


        </div>
    </div>
</div>
