<?php

use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\bootstrap\ActiveForm;

$this->title = 'Dashboard';
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="portfolio-index">
    <h1><?= Html::encode($this->title) ?>
    </h1>
    <style>
    hr.my_hr {
        margin: 5px 0;
    }
    </style>

    <div class="row">
        <div class="col-lg-6">
            <?php echo GridView::widget(
    [
                    'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <legend>Portfolio</legend>

                                    {items}


                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover'
                    ],
                    'dataProvider' => $assetProvider,
                    'showFooter' => true,

                    'columns' => [
                        [
                            'attribute' => 'name',
                            'footer' => 'Total Aseets:'
                                . '<br/><hr class="my_hr"/>'
                                .'Total Liabilities:'
                                . '<br/><hr class="my_hr"/>'
                                . 'Total:'
                            ,
                        ],
                        [
                            'attribute' => 'value',
                            'format' => 'currency',
                            'footer' => 
                                Yii::$app->formatter->asCurrency($totalValue) 
                                . '<br/><hr class="my_hr"/>'
                                .'('.Yii::$app->formatter->asCurrency($totalLiabilityValue) . ')'
                                . '<br/><hr class="my_hr"/>'
                                .''.Yii::$app->formatter->asCurrency($totalValue - $totalLiabilityValue) . ''

                            
                            ,
                        ],
                        [
                            'attribute' => 'share',
                            'format' => ['percent', 2],
                        ],
                        [
                            'attribute' => 'yield',
                            'format' => 'currency',
                            'footer' => Yii::$app->formatter->asCurrency($totalProfit),
                        ],
                    ]
                ]
);?>


        </div>

        <div class="col-lg-6">
            <?php
            echo GridView::widget(
    [
                    'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <legend>Profit</legend>

                                    {items}


                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover'
                    ],
                    'dataProvider' => $profitProvider,
                    'showFooter' => true,

                    'columns' => [
                        [
                            'attribute' => '0',
                            'label' => false,
                            'format' => 'raw',
                        ],
                        [
                            'attribute' => '1',
                            'label' => false,
                            'format' => 'raw',
                        ],
                        // [
                        //     'attribute' => 'week',
                        //     // 'format' => ['percent', 2],
                        // ],
                        // [
                        //     'attribute' => 'month',
                        //     // 'format' => ['percent', 2],
                        // ],
                        // [
                        //     'attribute' => 'year',
                        //     // 'format' => ['percent', 2],
                        // ],
                        // [
                        //     'attribute' => 'all',
                        //     // 'format' => ['percent', 2],
                        // ],
                        // [
                        //     'label' => 'YTD',
                        //     'attribute' => 'ytd',
                        //     // 'format' => ['percent', 2],
                        // ],
                    ]
                ]
);
            ?>
        </div>

    </div>

</div>

<hr />
<?php
$start_date = isset($_POST['start_date'])?$_POST['start_date']:date('Y-m-d', strtotime('today -30 days'));
$end_date = isset($_POST['end_date'])?$_POST['end_date']:date('Y-m-d', strtotime('today'));

$data = [];

$begin = new DateTime($start_date);
$end = new DateTime($end_date);
//to include last date
$end = $end->modify( '+1 day' );

$interval = new DateInterval('P1D');
$daterange = new DatePeriod($begin, $interval, $end);
foreach ($daterange as $date) {
    $date = $date->format("Y-m-d");

    $value1 = \app\models\Asset::getValueSum('asset', $date);
    $value2 = \app\models\Asset::getValueSum('liability', $date);
    $sumValue = $value1 - $value2;
    // $sumValue = 0;

    $data[$date] = (float)($sumValue);
}
?>

<?php $form = ActiveForm::begin(['id'=>'chart']); ?>
<?php
$addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;
echo '<label class="control-label">Date Range</label>';
echo '<div class="input-group drp-container">';
use kartik\daterange\DateRangePicker;

echo DateRangePicker::widget([
    //'callback' => 'function(){$("#chart").submit()}',
    'name'=>'picker',
    //'model'=>$model,
    //'attribute'=>'datetime_range',
    'startAttribute' => 'start_date',
    'endAttribute' => 'end_date',
    'value'=>$start_date.' - '.$end_date,
    'convertFormat'=>true,
    'pluginOptions'=>[
        'timePicker'=>false,
        //        'timePickerIncrement'=>30,
        'locale'=>[
            'format'=>'Y-m-d'
        ],
        'initRangeExpr' => true,
        'ranges' => [
//                Yii::t('app', "Today") => ["moment().startOf('day')", "moment()"],
//                Yii::t('app', "Yesterday") => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
            Yii::t('app', "Last {n} Days", ['n' => 7]) => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
            Yii::t('app', "Last {n} Days", ['n' => 30]) => ["moment().startOf('day').subtract(29, 'days')", "moment()"],
            Yii::t('app', "This Month") => ["moment().startOf('month')", "moment().endOf('month')"],
            Yii::t('app', "Last Month") => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
            Yii::t('app', "This Year") => ["moment().startOf('year')", "moment().endOf('year')"],
            Yii::t('app', "Last Year") => ["moment().subtract(1, 'year').startOf('year')", "moment().subtract(1, 'year').endOf('year')"],
        ],
    ],
    'pluginEvents' => [
//            "show.daterangepicker" => "function() { log("show.daterangepicker"); }",
//            "hide.daterangepicker" => "function() { log("hide.daterangepicker"); }",
        "apply.daterangepicker" => "function() { $(\"#chart\").submit() }",
//            "cancel.daterangepicker" => "function() { log("cancel.daterangepicker"); }",
    ],
]) . $addon;
echo '</div>'
;?>
<?php $form::end(); ?>


<div class="row">
    <div class="col-lg-12">

        <?php use miloschuman\highcharts\Highcharts;

echo Highcharts::widget([
            'options' => [
                'title' => ['text' => 'Porfolio value'],
                'xAxis' => [
                    'categories' => array_keys($data),
                ],
                'yAxis' => [
                    'title' => ['text' => 'Value']
                ],
                'series' => [
                    ['name' => 'Portfolio', 'data' => array_values($data)],

                ]
            ]
        ]);?>
    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <?php
        echo GridView::widget(
            [
                'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <legend>Events</legend>

                                    {items}


                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                'tableOptions' => [
                    'class' => 'table table-striped table-hover'
                ],
                'dataProvider' => $eventProvider,

                'columns' => [
                    [
                        'attribute' => 'title',
                    ],
                    [
                        'attribute' => 'text',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $return = $model->text;
                            return $return;
                        }
                    ],
                    'created_at:date',
                ]
            ]
        );
        ?>
    </div>


    <div class="col-lg-6">
        <?php
        echo GridView::widget(
            [
                'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <legend>News</legend>

                                    {items}


                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                'tableOptions' => [
                    'class' => 'table table-striped table-hover'
                ],
                'dataProvider' => $newsProvider,

                'columns' => [
                    [
                        'attribute' => 'title',
                    ],
                    [
                        'attribute' => 'text',
                    ],
                    'created_at:date',
                ]
            ]
        );
        ?>
    </div>
</div>