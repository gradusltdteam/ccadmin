<?php

namespace app\models;

use Yii;
use \yii\db\Expression;

class AssetPrice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
//    public static function tableName()
//    {
//        return 'comment';
//    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'datetime', 'format'=>'php:Y-m-d'],
            [['id'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord)
        {
            if(!$this->created_at) {
                $this->created_at = new Expression('NOW()');
            }
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }
}
