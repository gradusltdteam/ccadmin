<?php

// comment out the following two lines when deployed to production

$localConfigFile = __DIR__ . '/../config/local.php';
$localConfig = file_exists($localConfigFile) ? require($localConfigFile) : [];

defined('YII_DEBUG') or define('YII_DEBUG', (isset($localConfig['common']['params']['debug']) && $localConfig['common']['params']['debug']));

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');


(new yii\web\Application(yii\helpers\ArrayHelper::merge(
//        require(__DIR__ . '/../config/common.php'),
        require(__DIR__ . '/../config/web.php'),
        isset($localConfig['common']) ? $localConfig['common'] : [],
        isset($localConfig['web']) ? $localConfig['web'] : []
)))->run();
