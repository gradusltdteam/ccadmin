# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.17)
# Database: db_name
# Generation Time: 2020-04-10 08:13:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table asset
# ------------------------------------------------------------

DROP TABLE IF EXISTS `asset`;

CREATE TABLE `asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL,
  `is_auto_update` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `asset` WRITE;
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;

INSERT INTO `asset` (`id`, `status`, `name`, `type`, `is_auto_update`, `created_at`, `updated_at`)
VALUES
	(1,0,'Cash',0,0,'2020-03-03 09:23:46','2020-03-03 09:23:46'),
	(3,0,'XRP',0,1,'2020-03-19 13:58:53','2020-04-09 23:49:34'),
	(4,0,'AAPL',1,0,'2020-03-19 13:59:05','2020-04-10 01:04:19'),
	(5,0,'BTC',0,0,'2020-03-19 16:14:50','2020-04-10 01:04:10'),
	(6,0,'GOOG',1,1,'2020-04-07 22:51:39','2020-04-07 23:05:14'),
	(7,0,'FAKE_NAME',1,0,'2020-04-09 21:17:28','2020-04-10 01:03:56'),
	(10,0,'RUB',0,1,'2020-04-10 00:32:34','2020-04-10 00:58:18');

/*!40000 ALTER TABLE `asset` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table asset_price
# ------------------------------------------------------------

DROP TABLE IF EXISTS `asset_price`;

CREATE TABLE `asset_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `amount` decimal(15,8) NOT NULL DEFAULT '0.00000000',
  `comment` varchar(255) DEFAULT NULL,
  `created_at` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `asset_price` WRITE;
/*!40000 ALTER TABLE `asset_price` DISABLE KEYS */;

INSERT INTO `asset_price` (`id`, `asset_id`, `amount`, `comment`, `created_at`)
VALUES
	(1,3,445.00000000,NULL,'2020-01-02'),
	(3,4,1234.00000000,NULL,'2020-03-18'),
	(4,4,555.00000000,NULL,'2020-03-19'),
	(5,4,111.00000000,NULL,'2020-03-19'),
	(6,4,6666.00000000,NULL,'2020-03-11'),
	(7,3,3333.00000000,NULL,'2020-03-20'),
	(8,3,559.00000000,NULL,'2020-04-07'),
	(10,3,1111.00000000,NULL,'2020-03-18'),
	(12,6,12345.00000000,NULL,'2020-04-07'),
	(22,3,51794.10000000,NULL,'2020-04-08'),
	(23,4,119.29000000,NULL,'2020-04-08'),
	(24,5,51866.32000000,NULL,'2020-04-08'),
	(25,6,119.29000000,NULL,'2020-04-08'),
	(26,5,7281.66000000,NULL,'2020-04-09'),
	(27,4,267.99000000,NULL,'2020-04-09'),
	(28,6,1211.45000000,NULL,'2020-04-09'),
	(29,3,0.20000000,NULL,'2020-04-09'),
	(30,3,0.19831000,NULL,'2020-04-10'),
	(31,4,267.99000000,NULL,'2020-04-10'),
	(32,5,7284.33000000,NULL,'2020-04-10'),
	(33,6,1211.45000000,NULL,'2020-04-10'),
	(34,10,0.01340700,NULL,'2020-04-10');

/*!40000 ALTER TABLE `asset_price` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;

INSERT INTO `event` (`id`, `created_at`, `title`, `text`)
VALUES
	(1,'2020-03-03 00:00:00','ewew','');

/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table log_price_api_update
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log_price_api_update`;

CREATE TABLE `log_price_api_update` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `asset_id` int(11) NOT NULL,
  `url` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `result` text,
  `api_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

LOCK TABLES `log_price_api_update` WRITE;
/*!40000 ALTER TABLE `log_price_api_update` DISABLE KEYS */;

INSERT INTO `log_price_api_update` (`id`, `status`, `created_at`, `asset_id`, `url`, `result`, `api_key`)
VALUES
	(93,0,'2020-04-09 21:23:34',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19790000\",\"6. Last Refreshed\":\"2020-04-09 21:23:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19789000\",\"9. Ask Price\":\"0.19790000\"}}',''),
	(94,0,'2020-04-09 21:23:36',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40390555\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(95,0,'2020-04-09 21:23:37',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7281.66000000\",\"6. Last Refreshed\":\"2020-04-09 21:23:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7282.17000000\",\"9. Ask Price\":\"7282.48000000\"}}',''),
	(96,0,'2020-04-09 21:23:39',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7351\",\"05. price\":\"1211.4500\",\"06. volume\":\"2158305\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(97,1,'2020-04-09 21:23:41',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=APPL&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(98,0,'2020-04-10 00:10:49',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19831000\",\"6. Last Refreshed\":\"2020-04-10 00:10:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19827000\",\"9. Ask Price\":\"0.19831000\"}}',''),
	(99,0,'2020-04-10 00:10:50',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(100,0,'2020-04-10 00:10:52',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7270.54000000\",\"6. Last Refreshed\":\"2020-04-10 00:10:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7270.54000000\",\"9. Ask Price\":\"7270.92000000\"}}',''),
	(101,0,'2020-04-10 00:10:54',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(102,1,'2020-04-10 00:10:58',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Note\":\"Thank you for using Alpha Vantage! Our standard API call frequency is 5 calls per minute and 500 calls per day. Please visit https:\\/\\/www.alphavantage.co\\/premium\\/ if you would like to target a higher API call frequency.\"}',''),
	(103,1,'2020-04-10 00:28:56',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(104,0,'2020-04-10 00:28:57',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19808000\",\"6. Last Refreshed\":\"2020-04-10 00:28:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19805000\",\"9. Ask Price\":\"0.19808000\"}}',''),
	(105,0,'2020-04-10 00:28:59',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(106,0,'2020-04-10 00:29:01',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7271.77000000\",\"6. Last Refreshed\":\"2020-04-10 00:29:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7271.80000000\",\"9. Ask Price\":\"7272.48000000\"}}',''),
	(107,0,'2020-04-10 00:29:02',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(108,1,'2020-04-10 00:51:48',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(109,1,'2020-04-10 00:51:51',10,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=RUR&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for CURRENCY_EXCHANGE_RATE.\"}',''),
	(110,0,'2020-04-10 00:51:53',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19830000\",\"6. Last Refreshed\":\"2020-04-10 00:51:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19829000\",\"9. Ask Price\":\"0.19830000\"}}',''),
	(111,0,'2020-04-10 00:51:54',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(112,0,'2020-04-10 00:51:56',5,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=BTC&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"BTC\",\"2. From_Currency Name\":\"Bitcoin\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"7284.33000000\",\"6. Last Refreshed\":\"2020-04-10 00:51:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"7284.33000000\",\"9. Ask Price\":\"7285.60000000\"}}',''),
	(113,1,'2020-04-10 00:59:02',7,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=FAKE_NAME&apikey=JZG02LP6DM8WG4I9','{\"Error Message\":\"Invalid API call. Please retry or visit the documentation (https:\\/\\/www.alphavantage.co\\/documentation\\/) for GLOBAL_QUOTE.\"}',''),
	(114,0,'2020-04-10 00:59:04',10,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=RUB&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"RUB\",\"2. From_Currency Name\":\"Russian Ruble\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.01342200\",\"6. Last Refreshed\":\"2020-04-10 00:59:03\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"-\",\"9. Ask Price\":\"-\"}}',''),
	(115,0,'2020-04-10 00:59:05',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(116,0,'2020-04-10 00:59:06',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19835000\",\"6. Last Refreshed\":\"2020-04-10 00:59:01\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19834000\",\"9. Ask Price\":\"0.19836000\"}}',''),
	(117,0,'2020-04-10 00:59:08',4,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=AAPL&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"AAPL\",\"02. open\":\"268.7000\",\"03. high\":\"270.0700\",\"04. low\":\"264.7000\",\"05. price\":\"267.9900\",\"06. volume\":\"40313106\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"266.0700\",\"09. change\":\"1.9200\",\"10. change percent\":\"0.7216%\"}}',''),
	(118,0,'2020-04-10 01:04:38',10,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=RUB&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"RUB\",\"2. From_Currency Name\":\"Russian Ruble\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.01340700\",\"6. Last Refreshed\":\"2020-04-10 01:04:38\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"-\",\"9. Ask Price\":\"-\"}}',''),
	(119,0,'2020-04-10 01:04:40',6,'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=GOOG&apikey=JZG02LP6DM8WG4I9','{\"Global Quote\":{\"01. symbol\":\"GOOG\",\"02. open\":\"1224.0800\",\"03. high\":\"1225.5700\",\"04. low\":\"1196.7400\",\"05. price\":\"1211.4500\",\"06. volume\":\"2155844\",\"07. latest trading day\":\"2020-04-09\",\"08. previous close\":\"1210.2800\",\"09. change\":\"1.1700\",\"10. change percent\":\"0.0967%\"}}',''),
	(120,0,'2020-04-10 01:04:41',3,'https://www.alphavantage.co/query?function=CURRENCY_EXCHANGE_RATE&from_currency=XRP&to_currency=USD&apikey=JZG02LP6DM8WG4I9','{\"Realtime Currency Exchange Rate\":{\"1. From_Currency Code\":\"XRP\",\"2. From_Currency Name\":\"Ripple\",\"3. To_Currency Code\":\"USD\",\"4. To_Currency Name\":\"United States Dollar\",\"5. Exchange Rate\":\"0.19831000\",\"6. Last Refreshed\":\"2020-04-10 01:04:05\",\"7. Time Zone\":\"UTC\",\"8. Bid Price\":\"0.19830000\",\"9. Ask Price\":\"0.19831000\"}}','');

/*!40000 ALTER TABLE `log_price_api_update` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table news
# ------------------------------------------------------------

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;

INSERT INTO `news` (`id`, `created_at`, `title`, `text`)
VALUES
	(1,'2020-03-03 00:00:00','ewew','');

/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table profit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `profit`;

CREATE TABLE `profit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table transfer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `transfer`;

CREATE TABLE `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(11) DEFAULT NULL,
  `amount` decimal(15,8) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `pair_transfer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `transfer` WRITE;
/*!40000 ALTER TABLE `transfer` DISABLE KEYS */;

INSERT INTO `transfer` (`id`, `asset_id`, `amount`, `comment`, `created_at`, `pair_transfer_id`)
VALUES
	(14,3,1.00000000,NULL,'2020-03-20 00:00:00',15),
	(15,1,-1000.00000000,NULL,'2020-03-20 00:00:00',14),
	(16,3,-0.50000000,NULL,'2020-03-20 00:00:00',17),
	(17,1,500.00000000,NULL,'2020-03-20 00:00:00',16),
	(18,1,1000.00000000,'i got $1000','2020-03-01 00:00:00',NULL),
	(20,1,500.00000000,'I got $500','2020-03-12 00:00:00',NULL);

/*!40000 ALTER TABLE `transfer` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
