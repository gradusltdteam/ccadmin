#!/usr/bin/env bash
##this script will assist with start/stop docker containers for current application

# Any subsequent(*) commands which fail will cause the shell script to exit immediately
set -e

#prevent script execution from invalid path
cd "$(dirname "${BASH_SOURCE[0]}")/.."

case $1 in
    start)
        docker-compose up -d
        # docker-compose exec -T sphinx indexer --all --rotate
        # docker-compose exec -T sphinx searchd
        ;;
    dev)
        docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d
        # docker-compose exec -T sphinx indexer --all --rotate
        # docker-compose exec -T sphinx searchd
        ;;
    stop)
        docker-compose stop
        ;;
    clean)
        docker-compose stop
        docker-compose rm -f
        ;;
    status)
        docker-compose ps
        ;;
    *)
        echo "Invalid command: $1"
        echo 'Valid commands: start|stop|clean|status'
        ;;
esac
