<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->params['logo'] ?? '',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-default navbar-fixed-top',
        ],
    ]);
    if (Yii::$app->user->isGuest) {
        $items = [
            // ['label' => 'Login', 'url' => ['/site/login']]
        ];
    } else {
        $items = [
            ['label' => 'Home', 'url' => ['/']],
            ['label' => \Yii::t('app', 'Portfolio'), 'url' => ['/portfolio/index']],
            ['label' => \Yii::t('app', 'Liabilities'), 'url' => ['/liability/index']],
//            ['label' => \Yii::t('app', 'Reports'), 'url' => ['/report']],
            ['label' => \Yii::t('app', 'News'), 'url' => ['/news']],
            ['label' => \Yii::t('app', 'Events'), 'url' => ['/event']],
            ['label' => \Yii::t('app', 'Users'), 'url' => ['/user/admin'], 'visible' => Yii::$app->user->can('admin')],
            ['label' => \Yii::t('app', 'Settings'), 'url' => ['/user/settings']],
            ['label' => \Yii::t('app', 'Clear cache'), 'url' => ['/site/clear-cache']],
            ('<li>'
                . Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>')
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $items,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?php echo Yii::$app->params['name'];?> <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
