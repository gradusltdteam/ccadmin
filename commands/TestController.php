<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TestController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionTimezone()
    {
        echo "  php:";
        echo date('Y-m-d H:i:s');
        echo " (" . date_default_timezone_get().")";
        echo "\n";
        echo "mysql:";
        $expression = new \yii\db\Expression('NOW()');
        $now = (new \yii\db\Query)->select($expression)->scalar();  // SELECT NOW();
        echo $now;
        
        $expression = new \yii\db\Expression('@@session.time_zone');
        $time_zone = (new \yii\db\Query)->select($expression)->scalar();
        echo " (" . $time_zone.")";
        echo "\n";
    }
}
