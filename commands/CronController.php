<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

class CronController extends Controller
{
    public function actionUpdateAssetPrice()
    {
        echo "begin:actionUpdateAssetPrice\n";
        $result = \app\models\Asset::updateAllApiPrice();
        print_r($result);
        echo "end:actionUpdateAssetPrice\n";
    }

    public function actionCleanup()
    {
        echo "begin:cleanup\n";

        \Yii::$app->getDb()->createCommand("
        DELETE FROM log_price_api_update WHERE created_at <= (NOW() - INTERVAL 1 WEEK)
        ")->execute();

        echo "end:cleanup\n";
    }


}
