<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		'css/admin.css',
    ];
    public $js = [
        'js/admin.js',
    ];
    public $depends = [
		'app\assets\AppAsset',
//		'app\assets\BootboxAsset',
        'app\assets\DatepickerAsset',
//        'app\assets\ResponsiveTable'
    ];
	public $jsOptions = [
	];
	public $cssOptions=[
	];
}