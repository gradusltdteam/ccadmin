<?php

use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use app\models\Transfer;
use app\models\Asset;


$this->title = 'Porfolio';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="price-update-info">
        <div class="row">
            <div class="col-lg-12">
                <?php
                    $log = \app\models\LogPriceApiUpdate::find()
                    ->limit(1)
                    ->orderBy('id DESC')
                    ->one()
                    // ->createCommand()->getRawSql()
                    ;
                    // var_dump($log);
                    // die('999');
                ?>
                <?php if($log):?>
                    Last time price updated: <?php echo $log->created_at?> (<?php echo Yii::$app->formatter->asRelativeTime($log->created_at)?>)<br/>
                <?php else:?>
                    No price updates<br/>
                <?php endif;?>
                
                <?php
                $error_count = \app\models\LogPriceApiUpdate::find()
                    ->where('status = '.\app\models\LogPriceApiUpdate::STATUS_ERROR.' AND created_at >= (NOW() - INTERVAL 24 HOUR)')
                    ->count();
                ?>
                <?php if($error_count):?>
                    <?php echo $error_count?> errors in API calls for last 24h!<br/>
                <?php else:?>
                <?php endif;?>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h2>Asset</h2>
            <?php
                echo GridView::widget(
                [
                    'layout' => "
                            <div class='panel panel-default'>
                                <div class='panel-body'>
                                    <div class='row'>
                                        <div class='col-md-12 text-right'>
                                            " . Html::a('<i class="fa fa-plus"></i> Add new', ['asset-update'],
                            ['class' => 'btn btn-success']) . "
                                        </div>
                                    </div>

                                    <hr>

                                    {items}


                                    <hr>
                                    <div class='row'>
                                        <div class='col-md-6'>{summary}</div>
                                        <div class='col-md-6 text-right'>{pager}</div>
                                    </div>
                                </div>
                            </div>

                    ",
                    'tableOptions' => [
                        'class' => 'table table-striped table-hover'
                    ],
                    'dataProvider' => $assetProvider,
                    'columns' => [
                        [
                            'class' => CheckboxColumn::className()
                        ],
                        'name',
                        'comment',
                        [
                            'attribute' => 'type',
                            'format' => 'raw',
                            'value' => function($model) {
                                return Asset::TYPE[$model->type];
                            }
                        ],
                        'amount:decimal',
                        'price:currency',
                        'value:currency',
                        [
                            'attribute' => 'share',
                            'format' => ['percent', 2],
                        ],
                        // 'created_at:date',
                        [
                            'header' => 'Price Updated',
                            'format' => 'raw',
                            'value' => function($model) {
                                $result = $model->is_auto_update ? HTML::tag('div', 'auto', [
                                    'class' => 'label label-info',
                                ]) . ' ' : '';

                                $log = \app\models\LogPriceApiUpdate::find()
                                ->where('asset_id = ' . $model->id)
                                ->orderBy('created_at DESC')
                                ->limit(1)
                                ->one();
                                
                                if($log) {
                                    if($log->status == \app\models\LogPriceApiUpdate::STATUS_ERROR) {
                                        $result .= HTML::tag('div', 'error ' . Yii::$app->formatter->format($log->created_at, 'relativeTime'), [
                                            'class' => 'label label-danger',
                                            'title' => $log->result,
                                        ]);
                                    } else {
                                        $result .= HTML::tag('div', Yii::$app->formatter->format($log->created_at, 'relativeTime'), [
                                            'class' => 'label label-default',
                                        ]);
                                    }
                                }

                                return $result;
            
                            }
                        ],
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{update} {delete} {transfer} {price}',
                            'contentOptions' => [
                                'class' => 'text-right'
                            ],
                            'buttons' => [
                                'update' => function ($url, $model, $key) {
                                    if($model->id === 1) return false;
                                    return Html::a('<i class="fa fa-edit"></i> Edit', [
                                        'asset-update',
                                        'id' => $model->id
                                    ], ['class' => 'btn btn-xs btn-primary']
                                    );
                                },
                                'delete' => function ($url, $model, $key) {
                                    if($model->id === 1) return false;
                                    return Html::a('<i class="fa fa-trash"></i> Delete', [
                                        'asset-delete',
                                        'id' => $model->id
                                    ], ['class' => 'btn btn-xs btn-danger delete-button', 'data-id' => $model->id]
                                    );
                                },
                                'profit' => function ($url, $model, $key) {
                                    if($model->id === 1) return false;
                                    return Html::a('<i class="fa fa-check"></i> Profit', [
                                        'profit',
                                        'id' => $model->id
                                    ], ['class' => 'btn btn-xs btn-primary', 'data-id' => $model->id]
                                    );
                                },
                                'transfer' => function ($url, $model, $key) {
                                    if($model->id === 1) return false;
                                    return Html::a('<i class="fa fa-check"></i> Transfer', [
                                        'transfer',
                                        'id' => $model->id
                                    ], ['class' => 'btn btn-xs btn-success', 'data-id' => $model->id]
                                    );
                                },
                                'price' => function ($url, $model, $key) {
                                    if($model->id === 1) return false;
                                    return Html::a('<i class="fa fa-check"></i> Price', [
                                        'price',
                                        'id' => $model->id
                                    ], ['class' => 'btn btn-xs btn-default', 'data-id' => $model->id]
                                    );
                                },
                            ]
                        ]
                    ]
                ]
            );
            ?>


        </div>
    </div>

    <h2>Cash total: <?php echo Yii::$app->formatter->asCurrency($cashSum)?></h2>

    <div class="row">
        <div class="col-lg-4">
            <?php $model = $new_cash?>
            <?php $form = ActiveForm::begin([]); ?>
            <div class="panel panel-default">
                <div class="panel-body">
                    <legend>Cash Deposit/Withdraw</legend>
                    <?= $form->field($model, 'amount_cash')->label(true)->textInput(['placeholder' => 'Amount USD']) ?>
                    <?= $form->field($model, 'comment')->label(true)->textInput(['placeholder' => 'Comment']) ?>
                    <?= $form->field($model, 'created_at')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter date ...'],
                        'pluginOptions' => [
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'autoclose'=>true,
                            'format' => 'yyyy-mm-dd',
                        ]
                    ]);?>

                    <hr>
                    <div class="text-center">
                        <div class="btn-group" role="group">
                            <?= Html::submitButton('<i class="fa fa-check"></i> Submit', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?php $form::end(); ?>
            </div>
        </div>

        <div class="col-lg-8">

            <?php $script = <<< JS
                $(function(){
                    $('.btn-ajax-modal').click(function (){
                        var elm = $(this),
                            target = elm.attr('data-target'),
                            ajax_body = elm.attr('value');

                        $(target).modal('show')
                            .find('.modal-content')
                            .load(ajax_body);
                    });

                });
JS;
            $this->registerJs($script, yii\web\View::POS_READY);
            ?>

            <?php echo $this->context->renderPartial('_cash_history', ['dataProvider' => $transferProvider]);?>

        </div>
    </div>
</div>
